import CartParser from './CartParser';
import fs from 'fs';

let parser;

beforeEach(() => {
  parser = new CartParser();
});

describe('CartParser - unit tests', () => {
  test('validate 0 errors for correct data', () => {
    // given
    const data = `Product name ,Price , Quantity
    Mollis consequat , 9.00 , 2
    Scelerisque lacinia,18.90,1`;
    // when
    const errors = parser.validate(data);
    // then
    expect(errors.length).toBe(0);
  });

  test('validate Product name column in place', () => {
    // given
    const data = `Product,Price,Quantity
    Mollis consequat,9.00,2
    Scelerisque lacinia,18.90,1`;
    // when
    const errors = parser.validate(data);
    // then
    expect(errors).toEqual([
      {
        type: 'header',
        row: 0,
        column: 0,
        message:
          'Expected header to be named "Product name" but received Product.',
      },
    ]);
  });

  test('validate Price column in place', () => {
    // given
    const data = `Product name,Unit price,Quantity
    Mollis consequat,9.00,2
    Scelerisque lacinia,18.90,1`;
    // when
    const errors = parser.validate(data);
    // then
    expect(errors).toEqual([
      {
        type: 'header',
        row: 0,
        column: 1,
        message: 'Expected header to be named "Price" but received Unit price.',
      },
    ]);
  });

  test('validate Quantity column in place', () => {
    // given
    const data = `Product name,Price,Quantity to buy
    Mollis consequat,9.00,2
    Scelerisque lacinia,18.90,1`;
    // when
    const errors = parser.validate(data);
    // then
    expect(errors).toEqual([
      {
        type: 'header',
        row: 0,
        column: 2,
        message:
          'Expected header to be named "Quantity" but received Quantity to buy.',
      },
    ]);
  });

  test('validate missing row data', () => {
    // given
    const data = `Product name ,Price , Quantity
    Mollis consequat,9.00,2
    Scelerisque lacinia,18.90`;
    // when
    const errors = parser.validate(data);
    // then
    expect(errors).toEqual([
      {
        type: 'row',
        row: 2,
        column: -1,
        message: 'Expected row to have 3 cells but received 2.',
      },
    ]);
  });

  test('validate missing product name', () => {
    // given
    const data = `Product name ,Price , Quantity
    ,9.00,2
    Scelerisque lacinia,18.90,1`;
    // then
    expect(parser.validate(data)).toEqual([
      {
        type: 'cell',
        row: 1,
        column: 0,
        message: 'Expected cell to be a nonempty string but received "".',
      },
    ]);
  });

  test('validate negative price', () => {
    // given
    const data = `Product name ,Price , Quantity
    Mollis consequat , 9.00 , 2
    Scelerisque lacinia,-18.90,1`;
    // then
    expect(parser.validate(data)).toEqual([
      {
        type: 'cell',
        row: 2,
        column: 1,
        message: 'Expected cell to be a positive number but received "-18.90".',
      },
    ]);
  });

  test('validate negative quantity', () => {
    // given
    const data = `Product name ,Price , Quantity
    Mollis consequat , 9.00 , -2
    Scelerisque lacinia,18.90,1`;
    // then
    expect(parser.validate(data)).toEqual([
      {
        type: 'cell',
        row: 1,
        column: 2,
        message: 'Expected cell to be a positive number but received "-2".',
      },
    ]);
  });

  test('calculate correct total', () => {
    // given
    const items = [
      {
        id: '3e6def17-5e87-4f27-b6b8-ae78948523a9',
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
      },
      {
        id: '90cd22aa-8bcf-4510-a18d-ec14656d1f6a',
        name: 'Tvoluptatem',
        price: 10.32,
        quantity: 1,
      },
    ];
    // then
    expect(parser.calcTotal(items)).toBe(9 * 2 + 10.32 * 1);
  });

  test('all required json fields present', () => {
    // when
    const jsonLine = parser.parseLine('Mollis consequat,9.00,2');
    // then
    expect(Object.keys(jsonLine).sort()).toEqual([
      'id',
      'name',
      'price',
      'quantity',
    ]);
  });

  test('id is uuid', () => {
    // when
    const jsonLine = parser.parseLine('Mollis consequat,9.00,2');
    // then
    expect(jsonLine.id.length).toBe(36);
  });
});

describe('CartParser - integration test', () => {
  const testFile = './src/test.csv';
  beforeEach(done => {
    const data = `Product name,Price,Quantity\nMollis consequat,9.00,2,5\nScelerisque lacinia,18.90`;
    fs.writeFile(testFile, data, err => {
      if (err) {
        done(err);
      }
      done();
    });
  });
  afterEach(done => {
    fs.rm(testFile, err => {
      if (err) {
        done(err);
      }
      done();
    });
  });
  test('validation fail', done => {
    expect(() => parser.parse(testFile)).toThrow('Validation failed!');
    done();
  });
});
